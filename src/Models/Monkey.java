package Models;

import java.util.List;

public class Monkey {

    private String name;
    private List<Trick> tricks;

    public Monkey() {
    }

    public Monkey(String name, List<Trick> tricks) {
        this.name = name;
        this.tricks = tricks;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Trick> getTricks() {
        return this.tricks;
    }

    public void setTricks(List<Trick> tricks) {
        this.tricks = tricks;
    }

    public void executeTrick(Trick trick, Spectator spec) {
        if (!this.tricks.contains(trick))
            return;

        System.out.println(String.format("%s exécute le tour %s", this.name, trick.getName()));
        spec.reactToTrick(trick, name);
    }

}
