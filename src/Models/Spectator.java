package Models;

import Enum.TrickType;

public class Spectator {

    private String name;

    public Spectator() {
    }

    public Spectator(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void reactToTrick(Trick trick, String nameOfMonkey) {
        TrickType trickType = trick.getTrick();
        switch (trickType) {
            case Acrobatics:
                applaud(trick, nameOfMonkey);
                break;
            case Music:
                whistle(trick, nameOfMonkey);
                break;
        }
    }

    public void applaud(Trick trick, String nameOfMonkey) {
        System.out.println(String.format("%s applaudit pendant le tour d'acrobatie '%s' de %s", this.name,
                trick.getName(), nameOfMonkey));
    }

    public void whistle(Trick trick, String nameOfMonkey) {
        System.out.println(String.format("%s siffle pendant le tour de musique '%s' de %s", this.name, trick.getName(),
                nameOfMonkey));
    }

}
