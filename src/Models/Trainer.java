package Models;

public class Trainer {

    private final Monkey monkey;

    public Trainer(Monkey monkey) {
        this.monkey = monkey;
    }

    public final Monkey getMonkey() {
        return this.monkey;
    }

    public void performTheMonkeyTricks(Spectator spec) {
        for (Trick trick : monkey.getTricks()) {
            monkey.executeTrick(trick, spec);
        }
    }

}
