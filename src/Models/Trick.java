package Models;

import Enum.TrickType;

public class Trick {
    
    private String name;
    private TrickType trick;

    public Trick() {}

    public Trick(String name, TrickType trick) {
        this.name = name;
        this.trick = trick;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrickType getTrick() {
        return this.trick;
    }

    public void setTrick(TrickType trick) {
        this.trick = trick;
    }

}
