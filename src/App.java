import java.util.List;

import Enum.TrickType;
import Models.Monkey;
import Models.Spectator;
import Models.Trainer;
import Models.Trick;

public class App {
    public static void main(String[] args) throws Exception {
        
        Spectator spectator = new Spectator("Charles");

        Trick trick1 = new Trick("Marcher sur les mains", TrickType.Acrobatics);
        Trick trick2 = new Trick("Jouer de la flute", TrickType.Music);
        Trick trick3 = new Trick("Tourner sur la tête", TrickType.Acrobatics);
        Trick trick4 = new Trick("Jouer de la guitare", TrickType.Music);
        List<Trick> list1 = List.of(trick1, trick2);
        List<Trick> list2 = List.of(trick3, trick4);

        Monkey monkey1 = new Monkey("Singe 1", list1);
        Monkey monkey2 = new Monkey("Singe 2", list2);

        Trainer trainer1 = new Trainer(monkey1);
        Trainer trainer2 = new Trainer(monkey2);
        
        trainer1.performTheMonkeyTricks(spectator);
        trainer2.performTheMonkeyTricks(spectator);
            
    }
}
